import { Component } from '@angular/core';

@Component({
    selector: 'app-warning-alert',
    template: `
        <div class="warning-alert">A Warning-Alert Message!</div>
        <app-sub-warning-alert></app-sub-warning-alert>
        `,
    styles: [
        '.warning-alert { color: red; }'
    ]
})

export class WarningAlertComponent {
}