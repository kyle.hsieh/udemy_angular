import { Component } from '@angular/core';

@Component({
    selector: 'app-sub-warning-alert',
    template: '<div class="sub-warning-alert">A Sub-Warning-Alert Message!</div>',
    styles: [
        '.sub-warning-alert { color: darkred; }'
    ]
})

export class SubWarningAlertComponent {
}