import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubSuccessAlertComponent } from './sub-success-alert.component';

describe('SubSuccessAlertComponent', () => {
  let component: SubSuccessAlertComponent;
  let fixture: ComponentFixture<SubSuccessAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubSuccessAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubSuccessAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
