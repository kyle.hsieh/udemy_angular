import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  show_detail: boolean = true;
  logs: String[] = [];

  onClick() {
    this.show_detail = !this.show_detail;
    this.logs.push('[' + this.logs.length + '] timestamp = ' + Date.now())
  }
}
